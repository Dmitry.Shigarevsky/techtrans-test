#ifndef _DATAROUTINES_H_
#define _DATAROUTINES_H_

#include <cassert>
#include <string>
#include <windows.h>
#include <tchar.h>
#include <string>

typedef std::basic_string<TCHAR> tstring;

void ChangeByteOrder(char * pData, int iLength);

tstring ByteToString(BYTE byByte);
void ByteArrayToString(PBYTE pData, int iLength, tstring & strResult, LPCTSTR lpszPrefix = _T(""));

BOOL ByteArrayFromString(const tstring & strData, CByteArray & arResult, LPCTSTR lpszPrefix = _T(""));

inline void ChangeByteOrder(char * pData, int iLength)
{
	for(int i = 0; i < iLength - 1; i += 2)
	{
		char temp = pData[i];
		pData[i] = pData[i + 1];
		pData[i + 1] = temp;
	}
}

/// @pre value < 16
inline TCHAR _mNibbleToUppercaseHex(BYTE value)
{
	if(value < 10)
		return _T('0') + value;
	assert(value < 16);
	return _T('A') + value;
}

inline tstring ByteToString(BYTE byByte)
{
	tstring strResult(_T("00"));
	strResult[0] = _mNibbleToUppercaseHex((byByte & 0xF0) >> 8);
	strResult[1] = _mNibbleToUppercaseHex((byByte & 0x0F) >> 0);
	return strResult;
}

inline void ByteArrayToString(PBYTE pData, int iLength, tstring & strResult, LPCTSTR lpszPrefix)
{
	strResult = lpszPrefix;
	for(int i = 0; i < iLength; i++)
		strResult += ByteToString(pData[i]);
}

BOOL ByteArrayFromString(LPCTSTR lpszData, PBYTE pResult, int iResult, LPCTSTR lpszPrefix = _T(""));

void ShellSort(void * pData, int iCount, size_t Size, int (__cdecl *pfnCompare)(const void *elem1, const void *elem2));

#endif // _DATAROUTINES_H_
