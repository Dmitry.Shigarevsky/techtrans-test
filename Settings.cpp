#include "stdafx.h"
#include <cassert>
#include <cctype>
#include <algorithm>
#include <map>
#include <ios>
#include <fstream>
#include <share.h>
#include <tchar.h>
#include "Settings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSettings g_Settings;

IMPLEMENT_SERIAL(CSettings, CObject, 1)

CSettings::CSettings()
{
	m_dwPollingPeriod = 1000;
	m_bTestLoopback = FALSE;
	m_bShowSIOMessages = FALSE;
	m_bShowMessageErrors = FALSE;
	m_bShowCOMErrors = FALSE;
	m_strSettingsReportPath = _T("ugs.rep");

	m_nBufferSize = 0x90000;

	m_nIncomingPort = 11112;

	m_strCOMSetup = _T("COM1: baud=9600 data=8 parity=N stop=1");
	m_iCOMRttc = 0;
	m_iCOMWttc = 0;
	m_iCOMRit = -1;

	m_arPrefix.RemoveAll();

	m_wComposedType = 0x000003;
	m_wOutputComposedType = 0x0000;
	m_wCRC16Init = 0xFFFF;

	m_wCPAddr = 0x0000;
	m_wPUAddr = 0x0000;

	m_bUnpackAll = FALSE;
	m_bMarkAll = FALSE;

	m_nStatusPeriod = 0;
	m_iSendStatTO = 1000000;
	m_StatusHdr = MESSAGETYPE(0x0000, 0x20);
	m_StatusMsg = MESSAGETYPE(m_wComposedType);
	m_MarkNestedMask = MESSAGETYPE();
	m_MarkComposedMask = MESSAGETYPE();
	m_arStatusData.RemoveAll();
	MakeStatusMsg();
	UpdateStatusMsg(0);
	m_TUType = 0x000002;
	m_TUSrcMask = 0x0000;
	m_TUSrcComMsgIndex = FALSE;
	m_TUPrimToSecSrc = 1;
	m_TUSecToPrimSrc = 1;


	m_bKeepLog = FALSE;
	m_wLogComposedType = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wLogComposedTypeToPack = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wSourceID = 0x000020;
	m_wStatusRequestMessageType = 0x0001;

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes.SetAt(0x0001, typeStatus);
}

CSettings::~CSettings()
{
}

void replDef(unsigned short& expl, const unsigned short& def)
{
  expl = expl ? expl : def;
}


template<typename Key, typename Value, typename Compare, class Allocator>
static bool Lookup(
	const std::map<Key, Value, Compare, Allocator>& map,
	const typename std::map<Key, Value, Compare, Allocator>::key_type& key,
	typename std::map<Key, Value, Compare, Allocator>::mapped_type& value
)
{
	const std::map<Key, Value, Compare, Allocator>::const_iterator i = map.find(key);
	if(i == map.end())
		return false;
	value = i->second;
	return true;
}

#ifdef __cpp_lib_map_try_emplace
	#define _m_std_map_HAS_insert_or_assign __cpp_lib_map_try_emplace >= 201411L
#else
	#define _m_std_map_HAS_insert_or_assign __cplusplus >= 201703L
#endif
#ifdef __has_cpp_attribute
	#define _m_HAS_deprecated __has_cpp_attribute(deprecated) >= 201309L
#else
	#define _m_HAS_deprecated __cplusplus >= 201402L
#endif
#if _m_std_map_HAS_insert_or_assign && _m_HAS_deprecated
[[deprecated("Use std::map::insert_or_assign directly instead.")]]
#endif
template<typename Key, typename Value, typename Compare, class Allocator, typename M>
static std::pair<typename std::map<Key, Value, Compare, Allocator>::iterator, bool> SetAt(
	std::map<Key, Value, Compare, Allocator>& map,
	const typename std::map<Key, Value, Compare, Allocator>::key_type& key,
	const M& obj
)
{
#if _m_std_map_HAS_insert_or_assign
	return map.insert_or_assign(key, obj);
#else
	typedef std::map<Key, Value, Compare, Allocator> Map;
	const Map::iterator i = map.lower_bound(key);
	if (i == map.end() || map.key_comp()(key, i->first))
		return std::pair<Map::iterator, bool>(map.insert(i, Map::value_type(key, obj)), true);
	assert(!map.key_comp()(i->first, key));
	assert(!map.key_comp()(key, i->first));
	i->second = obj;
	return std::pair<Map::iterator, bool>(i, false);
#endif
}
#undef _m_HAS_deprecated
#undef _m_std_map_HAS_insert_or_assign

static tstring ToString(unsigned n) {
	if (!n)
		return _T("0");
	tstring s;
	while (n) {
		s += static_cast<TCHAR>(_T('0') + n%10);
		n /= 10;
	}
	std::reverse(s.begin(), s.end());
	return s;
}

static bool _m_not_space(const TCHAR c) {
	return !_istspace(c);
}

static tstring& Trim(tstring& str) {
	{	const tstring::reverse_iterator i = std::find_if(str.rbegin(), str.rend(), _m_not_space);
		str.erase(i.base(), str.end());
	}
	{	const tstring::iterator i = std::find_if(str.begin(), str.end(), _m_not_space);
		str.erase(str.begin(), i);
	}
	return str;
}

static tstring Trim(const tstring& str) {
	tstring copy = str;
	return Trim(copy);
}

static tstring& Trim(tstring& str, const PCTSTR targets) {
	{	const tstring::size_type i = str.find_last_not_of(targets);
		if (i == str.npos) {
			str.clear();
			return str;
		}
		assert(i < str.length());
		str.erase(i + 1, str.npos);
	}
	{	const tstring::size_type i = str.find_first_not_of(targets);
		assert(i < str.length());
		str.erase(0, i);
	}
	return str;
}

static tstring Tokenize(const tstring& str, const PCTSTR dels, tstring::size_type& pos) {
	const tstring::size_type i = str.find_first_not_of(dels, pos);
	const tstring::size_type j = str.find_first_of(dels, i);
	pos = str.find_first_not_of(dels, j);
	assert(j >= i);
	if (i <= str.length())
		return tstring(str, i, j - i);
	assert(i == str.npos);
	return tstring();
}

static tstring& MakeLower(tstring& str) {
	std::transform(str.begin(), str.end(), str.begin(), _totlower);
	return str;
}

static CString ToMfcString(const tstring& str) {
	return CString(str.c_str(), str.length());
}

BOOL CSettings::Load(LPCTSTR lpszProfileName)
{
	try
	{
		std::basic_ifstream<TCHAR> file(lpszProfileName, std::ios_base::in, _SH_DENYWR);

		std::map<tstring, tstring> mapSettings;
		tstring strGroup(_T("[General]"));

		tstring strLine;
		while(getline(file, strLine))
		{
			Trim(strLine);
			tstring::size_type iComment = strLine.find(_T(';'), 0);
			if(iComment != strLine.npos)
				strLine.erase(iComment, strLine.length() - iComment);

			if(strLine.empty())
				continue;

			if(strLine.find(_T('[')) == 0 && strLine.rfind(_T(']')) == strLine.length() - 1)
			{
				strGroup = strLine;
				continue;
			}

			tstring::size_type iPos = 0;
			tstring strKey(strGroup + _T('/') + Trim(Tokenize(strLine, _T("="), iPos)));
			tstring strValue(strLine);
			strValue.erase(0, iPos);

			SetAt(mapSettings, strKey, Trim(Trim(strValue), _T("\"")));
		}
		if (!file.eof()) {
			file.exceptions(file.failbit | file.badbit);
		}

		int iValue;
		if(Lookup(mapSettings, _T("[General]/PollingPeriod"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
			m_dwPollingPeriod = (DWORD)iValue;

		if(Lookup(mapSettings, _T("[General]/TestLoopback"), strLine))
			m_bTestLoopback = MakeLower(strLine) == _T("1") ? TRUE : FALSE;

		if(Lookup(mapSettings, _T("[General]/ShowSIOMessages"), strLine))
			m_bShowSIOMessages = MakeLower(strLine) == _T("1") ? TRUE : FALSE;

		if(Lookup(mapSettings, _T("[General]/ShowMessageErrors"), strLine))
			m_bShowMessageErrors = MakeLower(strLine) == _T("1") ? TRUE : FALSE;

		if(Lookup(mapSettings, _T("[General]/ShowCOMErrors"), strLine))
			m_bShowCOMErrors = MakeLower(strLine) == _T("1") ? TRUE : FALSE;

		if(Lookup(mapSettings, _T("[General]/SettingsReportPath"), strLine))
			m_strSettingsReportPath = ToMfcString(strLine);

		if(Lookup(mapSettings, _T("[General]/BufferSize"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue) && iValue != 0)
			m_nBufferSize = (UINT)iValue;

		if(Lookup(mapSettings, _T("[UDP]/IncomingPort"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue) && iValue != 0)
			m_nIncomingPort = (UINT)iValue;

		if(Lookup(mapSettings, _T("[UDP]/OutgoingIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine.c_str());
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine.c_str());
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}

		}


		if(Lookup(mapSettings, _T("[COM]/SetupParams"), strLine))
			m_strCOMSetup = ToMfcString(strLine);

		if(Lookup(mapSettings, _T("[COM]/rttc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMRttc = iValue;

		if(Lookup(mapSettings, _T("[COM]/wttc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMWttc = iValue;

		if(Lookup(mapSettings, _T("[COM]/rit"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_iCOMRit = iValue;

		CByteArray arTemp;
		if(Lookup(mapSettings, _T("[Message]/CPAddr"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wCPAddr = (WORD)iValue;
		if(Lookup(mapSettings, _T("[Message]/PUAddr"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wPUAddr = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Message]/Prefix"), strLine) && ByteArrayFromString(strLine, arTemp, _T("")))
			m_arPrefix.Copy(arTemp);

		if(Lookup(mapSettings, _T("[Message]/OutPrefix"), strLine) && ByteArrayFromString(strLine, arTemp, _T("")))
			m_arOutPrefix.Copy(arTemp);

		if(Lookup(mapSettings, _T("[Message]/CRC16Init"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wCRC16Init = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Message]/ComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wComposedType = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Message]/OutputComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wOutputComposedType = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Message]/TypesToUnPack"), strLine))
		{
			m_mapMsgTypesToUnpack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToUnpack.SetAt(0x0000, NULL);
				m_bUnpackAll = TRUE;
			}
			else
			{
				for(tstring::size_type iPos = 0; iPos < strLine.length() - 1; )
				{
					if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToUnpack.SetAt((WORD)iValue, NULL);
				}
				m_bUnpackAll = FALSE;
			}
		}

		if(Lookup(mapSettings, _T("[Message]/MarkComposedMessageMask"), strLine))
		{
			tstring::size_type iPos = 0;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wSrcMask = (WORD)iValue;
		}
		if(Lookup(mapSettings, _T("[Message]/MarkMessageMask"), strLine))
		{
			tstring::size_type iPos = 0;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wSrcMask = (WORD)iValue;
		}

		if(Lookup(mapSettings, _T("[Message]/TypesToMark"), strLine))
		{
			m_mapMsgTypesToMark.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToMark.SetAt(0x0000, NULL);
				m_bMarkAll = TRUE;
			}
			else
			{
				for(tstring::size_type iPos = 0; iPos < strLine.length() - 1; )
				{
					if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToMark.SetAt((WORD)iValue, NULL);
				}
				m_bMarkAll = FALSE;
			}
		}

		m_mapMsgTypes.RemoveAll();

		for(int i = 1; i < 10; i++)
		{
			tstring strTemp;
			strTemp = _T("[Message]/Type") + ToString(i);

			if(!Lookup(mapSettings, strTemp, strLine))
				continue;

			MESSAGETYPE type;
			tstring::size_type iPos = 0;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wType = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wMaxLength = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wSource = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				type.m_wSrcMask = (WORD)iValue;

			if(type.m_wType == 0x0505) {
				replDef(type.m_wSource, m_wPUAddr);
				replDef(type.m_wDestination, m_wCPAddr);
			}
			else if(type.m_wType == 0x0521 || type.m_wType == 0x0532) {
				replDef(type.m_wSource, m_wCPAddr);
			}
			else {
				replDef(type.m_wSource, m_wCPAddr);
				replDef(type.m_wDestination, m_wPUAddr);
			}
			
			m_mapMsgTypes.SetAt(type.m_wType, type);            
		}

		MESSAGETYPE typeStatus(0x0001, 0x1000);
		m_mapMsgTypes.SetAt(0x0001, typeStatus);

		if(Lookup(mapSettings, _T("[Message]/StatusPeriod"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_nStatusPeriod = (UINT)iValue;

		if(Lookup(mapSettings, _T("[Message]/SendStatusTO"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_iSendStatTO = (int)iValue;

		if(Lookup(mapSettings, _T("[Message]/StatusMsg"), strLine))
		{
			tstring::size_type iPos = 0;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wType = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wSource = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wType = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wSource = (WORD)iValue;
			ByteArrayFromString(Tokenize(strLine, _T(" "), iPos), arTemp, _T("")); // TODO:
			m_arStatusData.Copy(arTemp);

			replDef(m_StatusMsg.m_wSource, m_wCPAddr);
			replDef(m_StatusMsg.m_wDestination, m_wPUAddr);

			MakeStatusMsg();
			UpdateStatusMsg(0);
		}

		if(Lookup(mapSettings, _T("[Message]/TUType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_TUType = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Message]/TUSrcMask"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_TUSrcMask = (WORD)iValue;
		if(Lookup(mapSettings, _T("[Message]/TUSrcComMsgIndex"), strLine))
			m_TUSrcComMsgIndex = MakeLower(strLine) == _T("1") ? TRUE : FALSE;
		if(Lookup(mapSettings, _T("[Message]/TUPrimToSecSrc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_TUPrimToSecSrc = (UINT)iValue;
		if(Lookup(mapSettings, _T("[Message]/TUSecToPrimSrc"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_DEFAULT, &iValue))
			m_TUSecToPrimSrc = (UINT)iValue;

		if(Lookup(mapSettings, _T("[Log]/KeepLog"), strLine))
			m_bKeepLog = MakeLower(strLine) == _T("1") ? TRUE : FALSE;

		if(Lookup(mapSettings, _T("[Log]/LogIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine.c_str());
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine.c_str());
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}
		}


		if(Lookup(mapSettings, _T("[Log]/LogComposedType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedType = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Log]/LogTypesToUnPack"), strLine))
		{
			m_mapLogMsgTypesToUnpack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToUnpack.SetAt(0x0000, NULL);
				m_bLogUnpackAll = TRUE;
			}
			else
			{
				for(tstring::size_type iPos = 0; iPos < strLine.length() - 1; )
				{
					if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToUnpack.SetAt((WORD)iValue, NULL);
				}
				m_bLogUnpackAll = FALSE;
			}
		}

		if(Lookup(mapSettings, _T("[Log]/LogComposedTypeToPack"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedTypeToPack = (WORD)iValue;


		if(Lookup(mapSettings, _T("[Log]/LogTypesToPack"), strLine))
		{
			m_mapLogMsgTypesToPack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToPack.SetAt(0x0000, NULL);
				m_bLogPackAll = TRUE;
			}
			else
			{
				for(tstring::size_type iPos = 0; iPos < strLine.length() - 1; )
				{
					if(::StrToIntEx(Tokenize(strLine, _T(" "), iPos).c_str(), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToPack.SetAt((WORD)iValue, NULL);
				}
				m_bLogPackAll = FALSE;
			}
		}

		if(Lookup(mapSettings, _T("[Status]/SourceIndex"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wSourceID = (WORD)iValue;

		if(Lookup(mapSettings, _T("[Status]/StatusRequestMessageType"), strLine) && ::StrToIntEx(strLine.c_str(), STIF_SUPPORT_HEX, &iValue))
			m_wStatusRequestMessageType = (WORD)iValue;

		return TRUE;
	}
	catch(const std::ios_base::failure & e) {
		(void)e;
	}

	return FALSE;
}

struct OStreamFlagsRAII {
	std::basic_ostream<TCHAR>& os;
	const std::ios_base::fmtflags old_flags;

	explicit OStreamFlagsRAII(
		std::basic_ostream<TCHAR>& os_,
		const std::ios_base::fmtflags flags,
		const std::ios_base::fmtflags mask
	)
		: os(os_)
		, old_flags(os.setf(flags, mask))
	{ }

	~OStreamFlagsRAII() {
		os.flags(old_flags);
	}
};

template<typename T>
struct AsHex {
	T raw;
};

template<typename T>
static std::basic_ostream<TCHAR>& operator <<(std::basic_ostream<TCHAR>& os, const AsHex<T> value) {
	const OStreamFlagsRAII _m_flags_raii(os, os.hex, os.basefield);
	return os << value.raw;
}

template<typename T>
struct AsWidth4 {
	T wrapped;
};

template<typename T>
static std::basic_ostream<TCHAR>& operator <<(std::basic_ostream<TCHAR>& os, const AsWidth4<T> value) {
	os.width(4);
	return os << value.wrapped;
}

template<typename T>
static AsHex<T> asX(const T& raw) {
	const AsHex<T> result = {raw};
	return result;
}

template<typename T>
static AsWidth4<AsHex<T>> as04X(const T& raw) {
	const AsWidth4<AsHex<T>> result = {{raw}};
	return result;
}

BOOL CSettings::Save(LPCTSTR lpszProfileName)
{
	try
	{
		std::basic_ofstream<TCHAR> file(lpszProfileName, std::ios_base::out | std::ios_base::trunc, _SH_DENYWR);
		file.exceptions(file.failbit | file.badbit);
		file.setf(file.internal, file.adjustfield);
		file.setf(file.uppercase);
		file.fill(_T('0'));

		tstring strTemp;
		file << _T("[General]\n");
		file << _T("PollingPeriod = ") << m_dwPollingPeriod << _T("\n");
		file << _T("TestLoopback = ") << (m_bTestLoopback ? _T("1") : _T("0")) << _T("\n");
		file << _T("ShowSIOMessages = ") << (m_bShowSIOMessages ? _T("1") : _T("0")) << _T("\n");
		file << _T("ShowMessageErrors = ") << (m_bShowMessageErrors ? _T("1") : _T("0")) << _T("\n");
		file << _T("ShowCOMErrors = ") << (m_bShowCOMErrors ? _T("1") : _T("0")) << _T("\n");
		file << _T("SettingsReportPath = \"") << m_strSettingsReportPath << _T("\"\n");
		file << _T("BufferSize = 0x") << as04X(m_nBufferSize) << _T("\n");
		file << _T("[UDP]\n");
		file << _T("IncomingPort = ") << m_nIncomingPort << _T("\n");
		file << _T("[COM]\n");
		file << _T("SetupParams = \"") << m_strCOMSetup << _T("\"\n");
		file << _T("rttc = ") << m_iCOMRttc << _T("\n");
		file << _T("wttc = ") << m_iCOMWttc << _T("\n");
		file << _T("rit = ") << m_iCOMRit << _T("\n");
		file << _T("[Message]\n");
		file << _T("CPAddr = 0x") << as04X(m_wCPAddr) << _T("\n");
		file << _T("PUAddr = 0x") << as04X(m_wPUAddr) << _T("\n");
		ByteArrayToString(m_arPrefix.GetData(), (int)m_arPrefix.GetSize(), strTemp, _T("Prefix = \""));
		file << strTemp << _T("\"\n");
		file << _T("CRC16Init = 0x") << as04X(m_wCRC16Init) << _T("\n");
		file << _T("ComposedType = 0x") << as04X(m_wComposedType) << _T("\n");
		file << _T("OutputComposedType = 0x") << as04X(m_wOutputComposedType) << _T("\n");
		file << _T("TypesToUnPack = \"");
		if (m_bUnpackAll)
			file << _T("*");
		else
		{
			POSITION pos = m_mapMsgTypesToUnpack.GetStartPosition();
			while(pos != NULL)
			{
				WORD wType;
				void * pTemp;
				m_mapMsgTypesToUnpack.GetNextAssoc(pos, wType, pTemp);
				file << as04X(wType) << _T(" ");
			}
		}
		file << _T("\"\n");

		file << _T("MarkComposedMessageMask  = \"0x") << as04X(m_MarkComposedMask.m_wDestMask) << _T(" 0x") << as04X(m_MarkComposedMask.m_wSrcMask) << _T("\"\n");
		file << _T("MarkMessageMask  = \"0x") << as04X(m_MarkNestedMask.m_wDestMask) << _T(" 0x") << as04X(m_MarkNestedMask.m_wSrcMask) << _T("\"\n");
		file << _T("TypesToMark = \"");
		if (m_bMarkAll)
			file << _T("*");
		else
		{
			POSITION pos = m_mapMsgTypesToMark.GetStartPosition();
			while(pos != NULL)
			{
				WORD wType;
				void * pTemp;
				m_mapMsgTypesToMark.GetNextAssoc(pos, wType, pTemp);
				file << as04X(wType) << _T(" ");
			}
		}
		file << _T("\"\n");

		POSITION pos = m_mapMsgTypes.GetStartPosition();
		for(int i = 0; pos != NULL && i < 10; i++)
		{
			WORD wType;
			MESSAGETYPE type;
			m_mapMsgTypes.GetNextAssoc(pos, wType, type);
			file << _T("Type") << i << _T(" = \"0x") << as04X(type.m_wType) << _T(" 0x") << asX(type.m_wMaxLength) << _T(" 0x") << as04X(type.m_wDestination)
				<< _T(" 0x") << as04X(type.m_wSource) << _T(" 0x") << as04X(type.m_wDestMask) << _T(" 0x") << as04X(type.m_wSrcMask) << _T("\"\n");
		}

		file << _T("StatusPeriod = ") << m_nStatusPeriod << _T("\n");
		file << _T("SendStatusTO = ") << m_iSendStatTO << _T("\n");
		file << _T("StatusMsg = \"0x") << as04X(m_StatusHdr.m_wType) << _T(" 0x") << as04X(m_StatusHdr.m_wDestination) << _T(" 0x") << as04X(m_StatusHdr.m_wSource)
			<< _T(" 0x") << as04X(m_StatusMsg.m_wType) << _T(" 0x") << as04X(m_StatusMsg.m_wDestination) << _T(" 0x") << as04X(m_StatusMsg.m_wSource) << _T(" ");
		ByteArrayToString(m_arStatusData.GetData(), (int)m_arStatusData.GetSize(), strTemp, _T(""));
		file << strTemp << _T("\"\n");

		file << _T("TUType = 0x") << as04X(m_TUType) << _T("\n");

		file << _T("TUSrcMask = 0x") << as04X(m_TUSrcMask) << _T("\n");
		file << _T("TUSrcComMsgIndex = ") << (m_TUSrcComMsgIndex ? _T("1") : _T("0")) << _T("\n");

		file << _T("TUPrimToSecSrc = ") << m_TUPrimToSecSrc << _T("\n");
		file << _T("TUSecToPrimSrc = ") << m_TUSecToPrimSrc << _T("\n");
		ByteArrayToString(m_arOutPrefix.GetData(), (int)m_arOutPrefix.GetSize(), strTemp, _T("OutPrefix = \""));
		file << strTemp << _T("\"\n");

		file << _T("[Log]\n");
		file << _T("KeepLog = ") << (m_bKeepLog ? _T("1") : _T("0")) << _T("\n");
		file << _T("LogComposedType = 0x") << as04X(m_wLogComposedType) << _T("\n");
		file << _T("LogTypesToUnPack = \"");
		if(m_bLogUnpackAll)
			file << _T("*");
		else
		{
			POSITION pos = m_mapLogMsgTypesToUnpack.GetStartPosition();
			while(pos != NULL)
			{
				WORD wType;
				void * pTemp;
				m_mapLogMsgTypesToUnpack.GetNextAssoc(pos, wType, pTemp);
				file << as04X(wType) << _T(" ");
			}
		}
		file << _T("\"\n");


		file << _T("LogComposedTypeToPack = 0x") << as04X(m_wLogComposedTypeToPack) << _T("\n");
		file << _T("LogTypesToPack = \"");
		if(m_bLogPackAll)
			file << _T("*");
		else
		{
			POSITION pos = m_mapLogMsgTypesToPack.GetStartPosition();
			while(pos != NULL)
			{
				WORD wType;
				void * pTemp;
				m_mapLogMsgTypesToPack.GetNextAssoc(pos, wType, pTemp);
				file << as04X(wType) << _T(" ");
			}
		}
		file << _T("\"\n");


		file << _T("[Status]\n");

		file << _T("SourceIndex = 0x") << as04X(m_wSourceID) << _T("\n");
		file << _T("StatusRequestMessageType = 0x") << as04X(m_wStatusRequestMessageType) << _T("\n");

		return TRUE;
	}
	catch(const std::ios_base::failure & e) {
		(void)e;
	}

	return FALSE;
}

void CSettings::MakeStatusMsg()
{
	m_StatusMsg.m_wMaxLength = 16 + (WORD)m_arStatusData.GetSize();
	m_StatusHdr.m_wMaxLength = 16 + m_StatusMsg.m_wMaxLength;
	m_arStatusMsg.SetSize(m_StatusHdr.m_wMaxLength);
	BYTE * pData = m_arStatusMsg.GetData();
	::ZeroMemory(pData, m_StatusHdr.m_wMaxLength);
	WORD * pHeader = (WORD *)pData;
	pHeader[0] = m_StatusHdr.m_wMaxLength;
	pHeader[1] = m_StatusHdr.m_wType;
	pHeader[2] = m_StatusHdr.m_wDestination;
	pHeader[3] = m_StatusHdr.m_wSource;
	pHeader[7] = m_StatusMsg.m_wMaxLength;
	pHeader[8] = m_StatusMsg.m_wType;
	pHeader[9] = m_StatusMsg.m_wDestination;
	pHeader[10] = m_StatusMsg.m_wSource;
	memcpy(pData + 28, m_arStatusData.GetData(), m_arStatusData.GetSize());
}

void CSettings::UpdateStatusMsg(unsigned char ind)
{
	BYTE * pData = m_arStatusMsg.GetData();
	UINT nLength = (UINT)m_arStatusMsg.GetSize();
	*((DWORD *)(pData + 8)) = *((DWORD *)(pData + 22)) = (DWORD)time(NULL);
	pData[12] = ind;
	pData[26] = ind;
	*((WORD *)(pData + nLength - sizeof(DWORD))) = CRC16(pData + 14, nLength - 14 - sizeof(DWORD), m_wCRC16Init);
	*((WORD *)(pData + nLength - sizeof(WORD))) = CRC16(pData, nLength - sizeof(WORD), m_wCRC16Init);
}
