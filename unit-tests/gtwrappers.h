#ifndef _h_GTWRAPPERS
#define _h_GTWRAPPERS

#include <gtest/gtest.h>

#ifdef GTEST_INIT_GOOGLE_TEST_NAME_ // GTest >= 1.8
	#define EXPECT_RELATION(ACTUAL, REL, EXPECTED) EXPECT_##REL(ACTUAL, EXPECTED)
	#define ASSERT_RELATION(ACTUAL, REL, EXPECTED) ASSERT_##REL(ACTUAL, EXPECTED)
#else
	#define EXPECT_RELATION(ACTUAL, REL, EXPECTED) EXPECT_##REL _m_GPRED_ORDER_##REL(ACTUAL, EXPECTED)
	#define ASSERT_RELATION(ACTUAL, REL, EXPECTED) ASSERT_##REL _m_GPRED_ORDER_##REL(ACTUAL, EXPECTED)
	#define _m_GPRED_ORDER_EQ(ACTUAL, EXPECTED) (EXPECTED, ACTUAL)
	#define _m_GPRED_ORDER_NE(ACTUAL, EXPECTED) (ACTUAL, EXPECTED)
	#define _m_GPRED_ORDER_LT(ACTUAL, EXPECTED) (ACTUAL, EXPECTED)
	#define _m_GPRED_ORDER_GT(ACTUAL, EXPECTED) (ACTUAL, EXPECTED)
	#define _m_GPRED_ORDER_LE(ACTUAL, EXPECTED) (ACTUAL, EXPECTED)
	#define _m_GPRED_ORDER_GE(ACTUAL, EXPECTED) (ACTUAL, EXPECTED)
#endif

struct _m_ThrowHelper {
private:
	static const ::testing::TestPartResult::Type _m_type = ::testing::TestPartResult::kFatalFailure;
	const char * const _m_file;
	const int _m_line;
	std::string _m_message;

public:
	_m_ThrowHelper(const char * const file, const int line, const char * const message)
		: _m_file(file)
		, _m_line(line)
		, _m_message(message)
	{
        _m_message += "\nInconclusive.";
	}

	::testing::AssertionException operator =(const ::testing::Message &message) const {
		::testing::internal::AssertHelper(_m_type, _m_file, _m_line, _m_message.c_str()) = message;
		return ::testing::AssertionException(::testing::TestPartResult(_m_type, _m_file, _m_line, _m_message.c_str()));
	}
};

#define _m_FATAL_THROW_MESSAGE(MESSAGE) throw _m_ThrowHelper(__FILE__, __LINE__, MESSAGE) = ::testing::Message()

#define INCONCLUSIVE() _m_FATAL_THROW_MESSAGE("(Unconditional)")
#define PREREQUIRE_FALSE(ACTUAL) GTEST_TEST_BOOLEAN_(!(ACTUAL), #ACTUAL, true, false, _m_FATAL_THROW_MESSAGE)
#define PREREQUIRE_TRUE( ACTUAL) GTEST_TEST_BOOLEAN_(  ACTUAL , #ACTUAL, false, true, _m_FATAL_THROW_MESSAGE)
#define PREREQUIRE_RELATION(ACTUAL, REL, EXPECTED) GTEST_PRED_FORMAT2_(::testing::internal::CmpHelper##REL, ACTUAL, EXPECTED, _m_FATAL_THROW_MESSAGE)

#endif // _h_GTWRAPPERS
