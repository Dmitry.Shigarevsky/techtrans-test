#include "stdafx.h"

#include <cstddef>
#include <cassert>
#include <cerrno>
#include <tchar.h>
#include <string.h>
#include <time.h>

#include <WinDef.h>
#include <Windows.h>

#include <algorithm>
#include <string>
#include <fstream>

#include "gtwrappers.h"

#define STRINGIFY(X) _m_STRINGIFY(X)
#define _m_STRINGIFY(X) #X

#define UNSIGNED_ERROR(NAME) (static_cast<DWORD>(ERROR_##NAME))

#if __cplusplus >= 201103L
	#define ALIGNAS(ALIGNMNET)          alignas(ALIGNMNET)
#else
	#define ALIGNAS(ALIGNMNET) __declspec(align(ALIGNMNET))
#endif

static const BOOL _mFALSE =   FALSE, _mTRUE =   TRUE;
#undef FALSE
#undef  TRUE
static const BOOL   FALSE = _mFALSE,   TRUE = _mTRUE;

namespace { namespace future { namespace std {
	template<typename T>
	struct type_identity {
		typedef T type;
	};

	template<typename It, typename Pred>
	It find_if_not(It first, const It last, const Pred pred) {
		for (; first != last; first++) {
			if (!pred(*first))
				return first;
		}
		assert(first == last);
		return last;
	}

	template<typename It, typename Size, typename UnaryFunction>
	It for_each_n(It first, Size n, UnaryFunction f) {
		while (n > 0) {
			f(*first);
			first++;
			n--;
		}
		return first;
	}
} } }

template<std::size_t n>
static std::ostream &write_z(std::ostream &os, const char (&s)[n]) { // TODO: [C++ >= C++17] Use std::string_view.
	typedef int _m_assert_non_empty[n ? +1 : -1];
	assert(s[n] == '\0');
	return os.write(s, n - 1);
}

static std::ostream &write(std::ostream &os, const std::string &s) {
	return os.write(s.c_str(), s.length());
}

#define FOR_EACH_MESSAGETYPE_FIELD(MACRO, SEP) \
	    MACRO(m_wType) \
	SEP MACRO(m_wMaxLength) \
	SEP MACRO(m_wDestination) \
	SEP MACRO(m_wSource) \
	SEP MACRO(m_wDestMask) \
	SEP MACRO(m_wSrcMask) \

static bool operator ==(const CSettings::MESSAGETYPE &lhs, const CSettings::MESSAGETYPE &rhs) {
#define _m_COMPARE(FIELD) lhs.FIELD == rhs.FIELD
	return FOR_EACH_MESSAGETYPE_FIELD(_m_COMPARE, &&);
#undef _m_COMPARE
}

static void PrintTo(const CSettings::MESSAGETYPE &value, std::ostream * const os) {
	os->put('{');
#define _m_PRINT(FIELD) \
	write_z(*os, "." #FIELD " = "); \
	write(*os, testing::PrintToString(value.FIELD));
	FOR_EACH_MESSAGETYPE_FIELD(_m_PRINT, write_z(*os, ", ");)
#undef _m_PRINT
	os->put('}');
}

static bool operator ==(const CByteArray &lhs, const CByteArray &rhs) {
	assert(lhs.GetSize() >= 0);
	assert(rhs.GetSize() >= 0);
	if (lhs.GetSize() != rhs.GetSize())
		return false;
	return std::equal(lhs.GetData(), lhs.GetData() + lhs.GetSize(), rhs.GetData());
}

static bool operator !=(const CByteArray &lhs, const CByteArray &rhs) {
	return !(lhs == rhs);
}

/// @pre d < 16
static char hex_digit_to_uppercase_char(const BYTE d) {
	if (d < 10)
		return '0' + d;
	assert(d < 16);
	return 'A' + (d - 10);
}

static void PrintTo(const CByteArray &bytes, std::ostream * const os) {
	assert(bytes.GetCount() >= 0);
	assert(os);

	const struct {
		std::ostream * const os;
		std::ostream &operator ()(const BYTE b) const {
			const char seq[] = {'\\', 'x', hex_digit_to_uppercase_char(b >> 4), hex_digit_to_uppercase_char(b & 0xF)};
			return os->write(seq, sizeof(seq)/sizeof(*seq));
		}
	#pragma warning(suppress: 4510 4610)
	} print_byte = {os}; // TODO: [C++ >= C++11] Use a lambda instead.

	os->put('"');
	future::std::for_each_n(bytes.GetData(), bytes.GetCount(), print_byte);
	os->put('"');
}

template<typename Traits>
static void PrintTo(const CStringT<char, Traits> &str, std::ostream * const os) { // TODO: CSimpleStringT<wchar_t>
	assert(str.GetLength() >= 0);
	assert(os);

	const struct {
		std::ostream * const os;
		std::ostream &operator ()(const char c) const {
			if (c == '"' || c == '\\')
				os->put('\\');
			return os->put(c);
		}
	#pragma warning(suppress: 4510 4610)
	} print_character = {os}; // TODO: [C++ >= C++11] Use a lambda instead.

	os->put('"');
	future::std::for_each_n(str.GetString(), str.GetLength(), print_character);
	os->put('"');
}

template<typename Key, typename ArgKey, typename Value, typename ArgValue>
static bool operator ==(const CMap<Key, ArgKey, Value, ArgValue> &lhs, const CMap<Key, ArgKey, Value, ArgValue> &rhs) {
	if (lhs.GetCount() != rhs.GetCount())
		return false;
	const CMap<Key, ArgKey, Value, ArgValue>::CPair *u = lhs.PGetFirstAssoc();
	while (u) {
		const CMap<Key, ArgKey, Value, ArgValue>::CPair * const v = rhs.PLookup(u->key);
		if (!v || u->value == v->key)
			return false;
		u = lhs.PGetNextAssoc(u);
	}
	return true;
}

template<typename Key, typename ArgKey, typename Value, typename ArgValue>
static void PrintTo(const CMap<Key, ArgKey, Value, ArgValue> &value, std::ostream * const os) {
	assert(os);
	os->put('{');
	const CMap<Key, ArgKey, Value, ArgValue>::CPair *u = value.PGetFirstAssoc();
	while (u) {
		write_z(*os, "[");
		write(*os, testing::PrintToString(u->key));
		write_z(*os, "] = ");
		write(*os, testing::PrintToString(u->value));
		u = value.PGetNextAssoc(u);
		if (u) {
			write_z(*os, ", ");
		}
	}
	os->put('}');
}

static bool operator ==(const CMapWordToPtr &lhs, const CMapWordToPtr &rhs) {
	if (lhs.GetCount() != rhs.GetCount())
		return false;
	POSITION p = lhs.GetStartPosition();
	while (p != NULL) {
		WORD k;
		void *u;
		lhs.GetNextAssoc(p, k, u);

		void *v;
		const BOOL found = rhs.Lookup(k, v);
		if (!found || u != v)
			return false;
	}
	return true;
}

static void PrintTo(const CMapWordToPtr &value, std::ostream * const os) {
	assert(os);
	os->put('{');
	POSITION p = value.GetStartPosition();
	while (p) {
		WORD k;
		void *v;
		value.GetNextAssoc(p, k, v);
		write_z(*os, "[");
		write(*os, testing::PrintToString(k));
		write_z(*os, "] = ");
		write(*os, testing::PrintToString(v));
		if (p) {
			write_z(*os, ", ");
		}
	}
	os->put('}');
}

namespace {

#define DEFINE_OPTIONAL_BASE(NAME, TYPE, ALIGNMENT) struct NAME { \
	typedef TYPE value_type; \
protected: \
	ALIGNAS(ALIGNMENT) unsigned char buffer[sizeof(value_type)]; \
private: \
	typedef int _m_assert_aligned[(ALIGNMENT)%__alignof(value_type) == 0 ? +1 : -1]; \
} \

struct _m_OptionalDefaultConstructionTag {
};

const _m_OptionalDefaultConstructionTag optional_default_construction_tag;

template<class Base>
struct Optional: Base {
private:
	bool _m_engaged;

public:
	/// @post !_m_engaged
	explicit Optional()
		: _m_engaged(false)
	{
	}

	explicit Optional(const _m_OptionalDefaultConstructionTag)
		: _m_engaged(true)
	{
		new (&buffer) value_type;
	}

	~Optional() {
		if (_m_engaged)
			reinterpret_cast<value_type &>(buffer).~value_type();
	}

	/// @pre !_m_engaged
	/// @post _m_engaged
	void default_initialize() {
		assert(!_m_engaged);
		new (&buffer) value_type;
		_m_engaged = true;
	}

	/// @pre _m_engaged
	/// @post !_m_engaged
	void finalize() {
		assert(_m_engaged);
		reinterpret_cast<value_type &>(buffer).~value_type();
		_m_engaged = false;
	}

	/// @pre _m_engaged
	typename Base::value_type &operator *() {
		assert(_m_engaged);
		return reinterpret_cast<value_type &>(buffer);
	}

	/// @pre _m_engaged
	typename Base::value_type *operator ->() {
		assert(_m_engaged);
		return reinterpret_cast<value_type *>(&buffer);
	}
};

struct File {
	const HANDLE handle;

	explicit File(const LPCTSTR filename
		, const DWORD desired_access = GENERIC_READ
		, const DWORD share_mode = FILE_SHARE_READ
		, const DWORD creation_disposition = OPEN_EXISTING
		, const DWORD flags_and_attributes = FILE_ATTRIBUTE_NORMAL
	)
		: handle(CreateFile(
			filename, desired_access, share_mode, NULL, creation_disposition, flags_and_attributes, NULL
		))
	{
	}

	~File() {
		if (handle == INVALID_HANDLE_VALUE)
			return;
		const BOOL closed = CloseHandle(handle);
		assert(closed != FALSE);
	}
};

std::string en_to_string(const int en) {
	char buffer[64000];
	if (const errno_t format_en = strerror_s(buffer, en))
		return "{failed to format message}";
	else
		return std::string(buffer);
}

std::string le_to_string(const DWORD le) {
	char buffer[64000];
	if (const DWORD n = FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, le, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		buffer, sizeof(buffer)/sizeof(*buffer), NULL
	))
		return std::string(buffer, n);
	else
		return "{failed to format message}";
}

template<typename Key, typename KeyArg, typename Value>
void SetAt(
	CMap<Key, KeyArg, Value, Value &> &map,
	typename future::std::type_identity<KeyArg>::type key, const Value &value
) {
	Value v = value;
	map.SetAt(key, v);
}

template<typename T, typename A, std::size_t n>
bool is_one_from(const T &t, const A (&a)[n])
{
	return std::find(a, a + n, t) != a + n;
}

typedef std::basic_string  <TCHAR> tstring  ;
typedef std::basic_ifstream<TCHAR> tifstream;

/// @pre pre_line.length() == post_line.length()
tstring::size_type longest_common_prefix(const tstring &pre_line, const tstring &post_line) {
	assert(pre_line.length() == post_line.length());
	const std::pair<tstring::const_iterator, tstring::const_iterator> p
		= std::mismatch(pre_line.begin(), pre_line.end(), post_line.begin());
	assert(p.first - pre_line.begin() == p.second - post_line.begin());
	return p.first - pre_line.begin();
}

void fill_with_some_data(CByteArray &a, const BYTE seed = 0) {
	const unsigned char n = 32;
	a.SetSize(n);
	for (unsigned char b = 0; b < n; b++) {
		a[b] = seed + b;
	}
}

template<typename T>
const T &binfield(const BYTE bytes[], const std::size_t i) {
	return *reinterpret_cast<const T *>(bytes + i);
}

namespace Tests {

class NonconstructedSettingsFixture: public ::testing::Test {
private:
	DEFINE_OPTIONAL_BASE(_m_OptionalSettingsBase, const CSettings, 4);
protected:
	Optional<_m_OptionalSettingsBase> settings;
};

TEST_F(NonconstructedSettingsFixture, constructor) {
	settings.default_initialize();
	SUCCEED();
}

TEST_F(NonconstructedSettingsFixture, constructor_FieldsAreOk) {
	settings.default_initialize();

#define _m_CHECK_FIELD(FIELD) if (settings->FIELD) EXPECT_RELATION(settings->FIELD, EQ, TRUE)
	_m_CHECK_FIELD(m_bTestLoopback);
	_m_CHECK_FIELD(m_bShowSIOMessages);
	_m_CHECK_FIELD(m_bShowMessageErrors);
	_m_CHECK_FIELD(m_bShowCOMErrors);
	_m_CHECK_FIELD(m_bUnpackAll);
	_m_CHECK_FIELD(m_bMarkAll);
	_m_CHECK_FIELD(m_TUSrcComMsgIndex);
	_m_CHECK_FIELD(m_bKeepLog);
	_m_CHECK_FIELD(m_bLogUnpackAll);
	_m_CHECK_FIELD(m_bLogPackAll);
#undef _m_CHECK_FIELD

#define _m_CHECK_FIELD(FIELD) settings->FIELD.AssertValid()
	_m_CHECK_FIELD(m_arPrefix);
	_m_CHECK_FIELD(m_arOutPrefix);
	_m_CHECK_FIELD(m_mapMsgTypes);
	_m_CHECK_FIELD(m_mapMsgTypesToUnpack);
	_m_CHECK_FIELD(m_mapMsgTypesToMark);
	_m_CHECK_FIELD(m_arStatusData);
	_m_CHECK_FIELD(m_arStatusMsg);
	_m_CHECK_FIELD(m_mapLogMsgTypesToUnpack);
	_m_CHECK_FIELD(m_mapLogMsgTypesToPack);
#undef _m_CHECK_FIELD
}

class NondestructedSettingsFixture: public ::testing::Test {
private:
	DEFINE_OPTIONAL_BASE(_m_OptionalSettingsBase, const CSettings, 4);
protected:
	Optional<_m_OptionalSettingsBase> settings;
	NondestructedSettingsFixture()
		: settings(optional_default_construction_tag)
	{
	}
};

TEST_F(NondestructedSettingsFixture, destructor) {
	settings.finalize();
	SUCCEED();
}

template<class BaseFixture>
class WithSettings: public BaseFixture {
protected:
	CSettings settings;
};

class SettingsFixture: public WithSettings<::testing::Test> {
};

#define _m_LOVE_AND_ASSERT_SUCCESS(SETTINGS, METHOD, FILENAME) do { \
	errno = 0; \
	SetLastError(0); \
	const BOOL METHOD##_is_ok = (SETTINGS).METHOD(FILENAME); \
	ASSERT_RELATION(METHOD##_is_ok, EQ, TRUE) << "Failed to " #METHOD ":" \
		" latest errno = " << errno << " [" << en_to_string(errno) << "]," \
		" latest GLE() = " << GetLastError() << " [" << le_to_string(GetLastError()) << "]." \
	; \
} while (false) \

#define LOAD_AND_ASSERT_SUCCESS(SETTINGS, FILENAME) _m_LOVE_AND_ASSERT_SUCCESS(SETTINGS, Load, FILENAME)
#define SAVE_AND_ASSERT_SUCCESS(SETTINGS, FILENAME) _m_LOVE_AND_ASSERT_SUCCESS(SETTINGS, Save, FILENAME)

class NonexistentFileFixture: public ::testing::Test {
protected:
	static const TCHAR filename[];
	NonexistentFileFixture() {
		if (!DeleteFile(filename)) {
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(FILE_NOT_FOUND))
				<< "Failed to pre-delete the file: " << le_to_string(GetLastError()) << ".";
		}
	}
};
const TCHAR NonexistentFileFixture::filename[] = _T(STRINGIFY(CONFIG)"/_m_nonexistent.ini");

class CreateDirectoryFixture: public ::testing::Test {
protected:
	static const TCHAR filename[];
	CreateDirectoryFixture() {
		if (!CreateDirectory(filename, NULL)) {
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(ALREADY_EXISTS))
				<< "Failed to pre-create a test directory: " << le_to_string(GetLastError()) << ".";
		}
	}
	void check(const File &holder) const {
		PREREQUIRE_RELATION(holder.handle, NE, INVALID_HANDLE_VALUE) << "Failed to take hold of the test directrory; GLE() == "
			<< GetLastError() << " [" << le_to_string(GetLastError()) << "].";
		{	BY_HANDLE_FILE_INFORMATION file_info;
			{	const BOOL ok = GetFileInformationByHandle(holder.handle, &file_info);
				PREREQUIRE_RELATION(ok, NE, FALSE) << "Failed to get file info; GLE() == "
					<< GetLastError() << " [" << le_to_string(GetLastError()) << "].";
			}
			PREREQUIRE_RELATION(file_info.dwFileAttributes, NE, INVALID_FILE_ATTRIBUTES);
			PREREQUIRE_RELATION(file_info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY, NE, 0U)
				<< "The created file is not a directory.";
		}
	}
};
const TCHAR CreateDirectoryFixture::filename[] = _T(STRINGIFY(CONFIG)"/_m_directory.ini");

class ReadNonexistentFileFixture: public NonexistentFileFixture {
protected:
	ReadNonexistentFileFixture()
		: NonexistentFileFixture()
	{
		{	const File emulator(filename, GENERIC_READ, FILE_SHARE_READ);
			PREREQUIRE_RELATION(emulator.handle, EQ, INVALID_HANDLE_VALUE) << "Failed to fail to open a non-existent file.";
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(FILE_NOT_FOUND))
				<< "Unexpected error: " << le_to_string(GetLastError()) << ".";
		}
	}
};

typedef WithSettings<ReadNonexistentFileFixture> ReadNonexistentFile;
TEST_F(ReadNonexistentFile, Load) {
	const BOOL loaded = settings.Load(filename);
	EXPECT_RELATION(loaded, EQ, FALSE);
}

class ReadDirectoryFixture: public CreateDirectoryFixture {
private:
	const File _m_holder;
protected:
	ReadDirectoryFixture()
		: CreateDirectoryFixture()
		, _m_holder(filename, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS)
	{
		check(_m_holder);
		{	const File emulator(filename);
			PREREQUIRE_RELATION(emulator.handle, EQ, INVALID_HANDLE_VALUE)
				<< "Failed to fail to open the test directrory as a regular file.";
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(ACCESS_DENIED))
				<< "Message of the actual error: " << le_to_string(GetLastError()) << ".";
		}
	}
};

typedef WithSettings<ReadDirectoryFixture> ReadableDirectory;
TEST_F(ReadableDirectory, Load) {
	const BOOL loaded = settings.Load(filename);
	EXPECT_RELATION(loaded, EQ, FALSE);
}

class EmptyFileFixture: public ::testing::Test {
private:
	const File _m_ensurer_and_holder;
protected:
	static const TCHAR filename[];
	EmptyFileFixture()
		: _m_ensurer_and_holder(filename, GENERIC_READ, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY)
	{
		PREREQUIRE_RELATION(_m_ensurer_and_holder.handle, NE, INVALID_HANDLE_VALUE)
			<< "Failed to open the file or take hold of it; GLE() == " << GetLastError()
			<< " [" << le_to_string(GetLastError()) << "].";
		{	LARGE_INTEGER size;
			{	const BOOL got_size = GetFileSizeEx(_m_ensurer_and_holder.handle, &size);
				PREREQUIRE_RELATION(got_size, NE, FALSE);
			}
			PREREQUIRE_RELATION(size.QuadPart, EQ, 0);
			PREREQUIRE_RELATION(size.HighPart, EQ, 0);
			PREREQUIRE_RELATION(size. LowPart, EQ, 0U);
		}
		{	const File emulator(filename);
			PREREQUIRE_RELATION(emulator.handle, NE, INVALID_HANDLE_VALUE) << "Failed to open the file; GLE() == "
				<< GetLastError() << " [" << le_to_string(GetLastError()) << "].";
		}
	}
};
const TCHAR EmptyFileFixture::filename[] = _T(STRINGIFY(CONFIG)"/_m_empty.ini");

typedef WithSettings<EmptyFileFixture> EmptyFile;
TEST_F(EmptyFile, Load) {
	LOAD_AND_ASSERT_SUCCESS(settings, filename);
}

class FileFixture: public ::testing::TestWithParam<const TCHAR *> {
private:
	const File _m_holder_and_emulator;
protected:
	FileFixture()
		: _m_holder_and_emulator(GetParam())
	{
		PREREQUIRE_RELATION(_m_holder_and_emulator.handle, NE, INVALID_HANDLE_VALUE) << "Failed to open the file; GLE() == "
			<< GetLastError() << " [" << le_to_string(GetLastError()) << "].";
	}
};

typedef WithSettings<FileFixture> TruncatedFile;
TEST_P(TruncatedFile, Load) {
	const BOOL loaded = settings.Load(GetParam());
	EXPECT_RELATION(loaded, EQ, FALSE);
}

INSTANTIATE_TEST_CASE_P(Load_Truncated, TruncatedFile, ::testing::Values(
	_T("data/truncated-header-1.ini"),
	_T("data/truncated-header-2.ini"),
	_T("data/truncated-key-value.ini"),
	_T("data/truncated-string-literal.ini")
));

static void check_loaded(const CSettings &settings, const bool check_com_setup = true) {
	typedef CSettings::MESSAGETYPE MESSAGETYPE;
#define _m_CHECK_FIELD(FIELD, VALUE) EXPECT_RELATION(settings.FIELD, EQ, VALUE)
	_m_CHECK_FIELD(m_dwPollingPeriod, 1000U); // loaded
	_m_CHECK_FIELD(m_bTestLoopback, TRUE); // loaded
	_m_CHECK_FIELD(m_bShowSIOMessages, FALSE); // loaded
	_m_CHECK_FIELD(m_bShowMessageErrors, FALSE); // loaded
	_m_CHECK_FIELD(m_bShowCOMErrors, FALSE); // loaded
	_m_CHECK_FIELD(m_strSettingsReportPath, _T("ugs.rep")); // loaded
	_m_CHECK_FIELD(m_nBufferSize, 0x1234U); // loaded
	_m_CHECK_FIELD(m_strIncomingAddress, _T("")); // defaulted
	_m_CHECK_FIELD(m_nIncomingPort, 11115U); // loaded
	if (check_com_setup) {
		_m_CHECK_FIELD(m_strCOMSetup, _T("COM1: baud=19200 data=8 parity=N stop=2")); // loaded
	}
	_m_CHECK_FIELD(m_iCOMRttc, 456); // loaded
	_m_CHECK_FIELD(m_iCOMWttc, 789); // loaded
	_m_CHECK_FIELD(m_iCOMRit, 123); // loaded
	_m_CHECK_FIELD(m_arPrefix, CByteArray()); // loaded
	_m_CHECK_FIELD(m_arOutPrefix, CByteArray()); // loaded
	_m_CHECK_FIELD(m_wComposedType, 0x0003); // loaded
	_m_CHECK_FIELD(m_wOutputComposedType, 0x0000); // loaded
	_m_CHECK_FIELD(m_wCRC16Init, 0xFFFF); // loaded
	_m_CHECK_FIELD(m_wCPAddr, 0x000000); // loaded
	_m_CHECK_FIELD(m_wPUAddr, 0x000000); // loaded
	{	CMap<WORD, WORD, MESSAGETYPE, MESSAGETYPE &> value;
		SetAt(value, 0x0001, MESSAGETYPE(0x0001, 0x1000)); // explicitly set in Load
		SetAt(value, 0x0002, MESSAGETYPE(0x0002, 0x1000, 0x0000, 0x0000, 0x0000, 0x0000)); // loaded
		SetAt(value, 0x0003, MESSAGETYPE(0x0003, 0x1000, 0x0000, 0x0000, 0x0000, 0x0000)); // loaded
		SetAt(value, 0x0004, MESSAGETYPE(0x0004, 0x1000, 0x0000, 0x0000, 0x0000, 0x0000)); // loaded
		SetAt(value, 0x0005, MESSAGETYPE(0x0005, 0x1000, 0x0000, 0x0000, 0x0000, 0x0000)); // loaded
		SetAt(value, 0x0006, MESSAGETYPE(0x0006, 0x1000, 0x0000, 0x0000, 0x0000, 0x0000)); // loaded
		_m_CHECK_FIELD(m_mapMsgTypes, value);
	}
	_m_CHECK_FIELD(m_mapMsgTypesToUnpack, CMapWordToPtr()); // loaded
	_m_CHECK_FIELD(m_bUnpackAll, FALSE); // explicitly set in Load
	{	CMapWordToPtr value;
		value.SetAt(0x0000, NULL);
		_m_CHECK_FIELD(m_mapMsgTypesToMark, value); // explicitly set in Load
	}
	_m_CHECK_FIELD(m_bMarkAll, TRUE); // explicitly set in Load
	_m_CHECK_FIELD(m_nStatusPeriod, 1000U); // loaded
	_m_CHECK_FIELD(m_iSendStatTO, 123456); // loaded
	{	MESSAGETYPE value;
		value.m_wType = 0x0000; // loaded
		value.m_wMaxLength = 32; // set in MakeStatusMsg
		value.m_wDestination = 0x0000; // loaded
		value.m_wSource = 0x0000; // loaded
		value.m_wDestMask = 0; // from default argument of tagMESSAGETYPE
		value.m_wSrcMask = 0; // from default argument of tagMESSAGETYPE
		_m_CHECK_FIELD(m_StatusHdr, value);
	}
	{	MESSAGETYPE value = MESSAGETYPE(0x000003);
		value.m_wType = 0x0003; // loaded
		value.m_wMaxLength = 16; // set in MakeStatusMsg
		value.m_wDestination = 0x0000; // loaded
		value.m_wSource = 0x0000; // loaded
		value.m_wDestMask = 0; // replDef in Load
		value.m_wSrcMask = 0; // replDef in Load
		_m_CHECK_FIELD(m_StatusMsg, value);
	}
	{	MESSAGETYPE value = MESSAGETYPE();
		value.m_wType = 0; // from default argument of tagMESSAGETYPE
		value.m_wMaxLength = 0; // from default argument of tagMESSAGETYPE
		value.m_wDestination = 0; // from default argument of tagMESSAGETYPE
		value.m_wSource = 0; // from default argument of tagMESSAGETYPE
		value.m_wDestMask = 0x0000; // loaded
		value.m_wSrcMask = 0x0000; // loaded
		_m_CHECK_FIELD(m_MarkNestedMask, value);
	}
	{	MESSAGETYPE value = MESSAGETYPE();
		value.m_wType = 0; // from default argument of tagMESSAGETYPE
		value.m_wMaxLength = 0; // from default argument of tagMESSAGETYPE
		value.m_wDestination = 0; // from default argument of tagMESSAGETYPE
		value.m_wSource = 0; // from default argument of tagMESSAGETYPE
		value.m_wDestMask = 0x0000; // loaded
		value.m_wSrcMask = 0x0000; // loaded
		_m_CHECK_FIELD(m_MarkComposedMask, value);
	}
	_m_CHECK_FIELD(m_TUType, 0x0002); // loaded
	_m_CHECK_FIELD(m_TUSrcMask, 0x0000); // loaded
	_m_CHECK_FIELD(m_TUSrcComMsgIndex, FALSE); // loaded
	_m_CHECK_FIELD(m_TUPrimToSecSrc, 1U); // loaded
	_m_CHECK_FIELD(m_TUSecToPrimSrc, 1U); // loaded
	_m_CHECK_FIELD(m_arStatusData, CByteArray()); // loaded
#if 0 // time dependant
	_m_CHECK_FIELD(m_arStatusMsg, CByteArray()); // MakeStatusMsg + UpdateStatusMsg
#endif
	_m_CHECK_FIELD(m_bKeepLog, FALSE); // loaded
	_m_CHECK_FIELD(m_wLogComposedType, 0x0006); // loaded
	_m_CHECK_FIELD(m_mapLogMsgTypesToUnpack, CMapWordToPtr()); // loaded
	_m_CHECK_FIELD(m_bLogUnpackAll, FALSE); // explicitly set in Load
	_m_CHECK_FIELD(m_wLogComposedTypeToPack, 0x0000); // loaded
	{	CMapWordToPtr value;
		value.SetAt(0x0000, NULL); // explicitly set in Load
		_m_CHECK_FIELD(m_mapLogMsgTypesToPack, value);
	}
	_m_CHECK_FIELD(m_bLogPackAll, TRUE); // explicitly set in Load
	_m_CHECK_FIELD(m_wSourceID, 0x0030); // loaded
	_m_CHECK_FIELD(m_wStatusRequestMessageType, 0x0001); // loaded
#undef _m_CHECK_FIELD
}

typedef WithSettings<FileFixture> RegularFile;
TEST_P(RegularFile, Load) {
	LOAD_AND_ASSERT_SUCCESS(settings, GetParam());
	check_loaded(settings);
}

INSTANTIATE_TEST_CASE_P(Load_Regular, RegularFile, ::testing::Values(
	_T("data/regular.ini"),
	_T("data/with-comments-1.ini"),
	_T("data/with-comments-2.ini")
));

typedef WithSettings<FileFixture> FileWithSemicolon;
TEST_P(FileWithSemicolon, Load) {
	LOAD_AND_ASSERT_SUCCESS(settings, GetParam());
	check_loaded(settings, /* check_com_setup = */ false);
	ASSERT_RELATION(settings.m_strCOMSetup, EQ, _T("COM1: baud=19200; data=8; parity=N; stop=2"));
}

INSTANTIATE_TEST_CASE_P(Load_WithSemicolon, FileWithSemicolon, ::testing::Values(
	_T("data/with-semicolon.ini")
));

class WriteDirectoryFixture: public CreateDirectoryFixture {
private:
	const File _m_holder;
protected:
	WriteDirectoryFixture()
		: CreateDirectoryFixture()
		, _m_holder(filename, GENERIC_WRITE, FILE_SHARE_WRITE, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS)
	{
		check(_m_holder);
		{	const File emulator(filename, GENERIC_WRITE, 0);
			PREREQUIRE_RELATION(emulator.handle, EQ, INVALID_HANDLE_VALUE)
				<< "Failed to fail to open the test directrory as a regular file.";
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(ACCESS_DENIED))
				<< "Message of the actual error: " << le_to_string(GetLastError()) << ".";
		}
	}
};

typedef WithSettings<WriteDirectoryFixture> WritableDirectory;
TEST_F(WritableDirectory, Save) {
	const BOOL saved = settings.Save(filename);
	EXPECT_RELATION(saved, EQ, FALSE);
}

class WriteNonexistentFileFixture: public NonexistentFileFixture {
protected:
	WriteNonexistentFileFixture()
		: NonexistentFileFixture()
	{
		{	const File emulator(filename, GENERIC_WRITE, 0, CREATE_ALWAYS, FILE_FLAG_DELETE_ON_CLOSE);
			PREREQUIRE_RELATION(emulator.handle, NE, INVALID_HANDLE_VALUE)
				<< "Failed to create and open a non-existent test file: " << le_to_string(GetLastError()) << ".";
		}
		{	const DWORD attributes = GetFileAttributes(filename);
			PREREQUIRE_RELATION(attributes, EQ, INVALID_FILE_ATTRIBUTES) << "File shall not exist.";
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(FILE_NOT_FOUND))
				<< "Unexpected error: " << le_to_string(GetLastError()) << ".";
		}
	}
};

typedef WithSettings<WriteNonexistentFileFixture> WriteNonexistentFile;
TEST_F(WriteNonexistentFile, Save) {
	SAVE_AND_ASSERT_SUCCESS(settings, filename);
}

class PreexistentFileFixture: public ::testing::Test {
private:
	const File _m_ensurer_and_holder;
protected:
	static const TCHAR filename[];
	PreexistentFileFixture()
		: _m_ensurer_and_holder(filename, 0, FILE_SHARE_WRITE, OPEN_ALWAYS, FILE_ATTRIBUTE_TEMPORARY)
	{
		PREREQUIRE_RELATION(_m_ensurer_and_holder.handle, NE, INVALID_HANDLE_VALUE) << "Failed to create or open the file; GLE() == "
			<< GetLastError() << " [" << le_to_string(GetLastError()) << "].";
		{	const File emulator(filename, GENERIC_WRITE, 0);
			PREREQUIRE_RELATION(emulator.handle, NE, INVALID_HANDLE_VALUE)
				<< "Failed to open a test file: " << le_to_string(GetLastError()) << ".";
		}
	}
};
const TCHAR PreexistentFileFixture::filename[] = _T(STRINGIFY(CONFIG)"/_m_preexistent.ini");

typedef WithSettings<PreexistentFileFixture> PreexistentFile;
TEST_F(PreexistentFile, Save) {
	SAVE_AND_ASSERT_SUCCESS(settings, filename);
}

static const tstring sections[] = {
	_T("General"),
	_T("UDP"),
	_T("COM"),
	_T("Message"),
	_T("Log"),
	_T("Status"),
};

static void check_line(const unsigned lineno, const tstring &line) {
#define _m_OFFSET(I) lineno << ':' << ((I) - line.begin()) << ". "

	const tstring::const_iterator k = std::find_if(line.begin(), line.end(), _istgraph);
	EXPECT_RELATION(k - line.begin(), EQ, 0) << _m_OFFSET(line.begin())
		<< "This is the length of whitespace at the beginning of the line.";

	if (k == line.end())
		return;

	if (*k == _T('[')) {
		const tstring::const_iterator ke = k + 1;
		const tstring::const_iterator s = std::find_if(ke, line.end(), _istgraph);
		EXPECT_RELATION(s - ke, EQ, 0) << _m_OFFSET(ke) << "This is the length of whitespace after `[`.";
		ASSERT_RELATION(s, LT, line.end()) << _m_OFFSET(s) << "No section name.";
		const tstring::const_iterator se = future::std::find_if_not(s, line.end(), _istalnum);
		ASSERT_RELATION(se, GT, s) << _m_OFFSET(se) << "The section name is missing.";
		const tstring section(s, se);
		EXPECT_TRUE(is_one_from(section, sections)) << _m_OFFSET(s) << "Unknown section `" << section << "`.";
		const tstring::const_iterator c = std::find_if(se, line.end(), _istgraph);
		EXPECT_RELATION(c - se, EQ, 0) << _m_OFFSET(se) << "This is the length of whitespace after section name.";
		ASSERT_RELATION(c, LT, line.end()) << _m_OFFSET(c) << "No closing `]`.";
		ASSERT_RELATION(*c, EQ, _T(']')) << _m_OFFSET(c) << "Invalid character after section name.";
		const tstring::const_iterator ce = c + 1;
		const tstring::const_iterator l = std::find_if(ce, line.end(), _istgraph);
		ASSERT_RELATION(l, EQ, line.end()) << _m_OFFSET(l) << "Leftover character(s) after section header.";
		EXPECT_RELATION(l - ce, EQ, 0) << _m_OFFSET(ce) << "This is the length of whitespace after section header.";
		return;
	}

	const tstring::const_iterator ke = future::std::find_if_not(k, line.end(), _istalnum);
	ASSERT_RELATION(ke, GT, k) << _m_OFFSET(ke) << "The key is missing.";

	const tstring::const_iterator e = std::find_if(ke, line.end(), _istgraph);
	ASSERT_RELATION(e, LT, line.end()) << _m_OFFSET(e) << "The `=` and the value are missing.";
	ASSERT_RELATION(*e, EQ, _T('=')) << _m_OFFSET(e) << "Invalid character after the key.";
	EXPECT_RELATION(e - ke, EQ, 1) << _m_OFFSET(ke) << "This is the length of whitespace between key and `=`.";
	if (e == ke + 1)
		EXPECT_RELATION(*ke, EQ, _T(' ')) << _m_OFFSET(ke) << "Invalid whitespace between key and `=`.";

	const tstring::const_iterator ee = e + 1;

	const tstring::const_iterator v = std::find_if(ee, line.end(), _istgraph);
	ASSERT_RELATION(v, LT, line.end()) << _m_OFFSET(v) << "No value.";
	EXPECT_RELATION(v - ee, EQ, 1) << _m_OFFSET(ee) << "This is the length of whitespace between `=` and value.";
	if (v == ee + 1)
		EXPECT_RELATION(*ee, EQ, _T(' ')) << _m_OFFSET(ee) << "Invalid whitespace between `=` and value.";

	tstring::const_iterator ve;

	if (*v == _T('"')) {
		ve = v + 1;
		while (ve < line.end() && *ve != _T('"')) {
			if (*ve == _T('\\')) {
				ve++;
				if (ve == line.end()) {
					ADD_FAILURE() << _m_OFFSET(ve) << "Non-completed escape sequence.";
					break;
				}
			}
			ve++;
		}
		ASSERT_RELATION(ve, LT, line.end()) << _m_OFFSET(ve) << "Non-terminated string literal.";
		assert(*ve == _T('"'));
		ve++;
	} else if (_istalpha(*v)) {
		ve = future::std::find_if_not(v + 1, line.end(), _istalnum);
		const tstring value(v, ve);
		EXPECT_TRUE(value == _T("false") || value == _T("true")) << _m_OFFSET(v) << "Unknown keyword value.";
	} else {
		ve = future::std::find_if_not(v + 1, line.end(), _istgraph);
		{	TCHAR *p;
			(void)_tcstol(&*v, &p, 0);
			EXPECT_RELATION(p, EQ, &*v + (ve - v)) << _m_OFFSET(v) << "Value is expected to be a number but is not.";
		}
	}

	EXPECT_RELATION(line.end() - ve, EQ, 0) << _m_OFFSET(ve)
		<< "This is the number of leftover character(s) after value.";

#undef _m_OFFSET
}

class WriteFileAndScanBackFixture: public ::testing::Test {
private:
	const File _m_holder;
protected:
	static const TCHAR filename[];
	WriteFileAndScanBackFixture()
		: _m_holder(filename, 0, FILE_SHARE_WRITE)
	{
		if (_m_holder.handle == INVALID_HANDLE_VALUE)
			PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(FILE_NOT_FOUND))
				<< "Failed to open the file. Message: " << le_to_string(GetLastError()) << ".";
		{	const File checker(filename, GENERIC_WRITE, FILE_SHARE_WRITE);
			if (checker.handle == INVALID_HANDLE_VALUE)
				PREREQUIRE_RELATION(GetLastError(), EQ, UNSIGNED_ERROR(FILE_NOT_FOUND))
					<< "Failed to open the file. Message: " << le_to_string(GetLastError()) << ".";
		}
	}
};
const TCHAR WriteFileAndScanBackFixture::filename[] = _T(STRINGIFY(CONFIG)"/_m_checksyntax.ini");

typedef WithSettings<WriteFileAndScanBackFixture> WriteFileAndScanBack;
TEST_F(WriteFileAndScanBack, Save_CheckSyntaxAndSectionNames) {
	SAVE_AND_ASSERT_SUCCESS(settings, filename);
	tifstream file(filename);
	tstring line;
	for (std::streamoff lineno = 1; getline(file, line); lineno++) {
		assert(!file.bad());
		assert(!file.fail());
		check_line(lineno, line);
	}
	assert(file.is_open());
	PREREQUIRE_FALSE(file.bad()) << "I/O error.";
	assert(file.fail());
	PREREQUIRE_TRUE(file.eof()) << "Formatting error.";
}

class ReloadedSettingsFixture: public SettingsFixture {
protected:
	CSettings settings_reloaded;
};

TEST_F(ReloadedSettingsFixture, Save_Load_DefaultConstructedRoundtrip) {
	const TCHAR filename[] = _T(STRINGIFY(CONFIG)"/_m_tmp.ini");
	SAVE_AND_ASSERT_SUCCESS(settings, filename);
	LOAD_AND_ASSERT_SUCCESS(settings_reloaded, filename);
#define _m_CHECK_FIELD(FIELD) EXPECT_RELATION(settings_reloaded.FIELD, EQ, settings.FIELD)
	_m_CHECK_FIELD(m_dwPollingPeriod);
	_m_CHECK_FIELD(m_bTestLoopback);
	_m_CHECK_FIELD(m_bShowSIOMessages);
	_m_CHECK_FIELD(m_bShowMessageErrors);
	_m_CHECK_FIELD(m_bShowCOMErrors);
	_m_CHECK_FIELD(m_strSettingsReportPath);
	_m_CHECK_FIELD(m_nBufferSize);
	_m_CHECK_FIELD(m_strIncomingAddress);
	_m_CHECK_FIELD(m_nIncomingPort);
	_m_CHECK_FIELD(m_strCOMSetup);
	_m_CHECK_FIELD(m_iCOMRttc);
	_m_CHECK_FIELD(m_iCOMWttc);
	_m_CHECK_FIELD(m_iCOMRit);
	_m_CHECK_FIELD(m_arPrefix);
	_m_CHECK_FIELD(m_arOutPrefix);
	_m_CHECK_FIELD(m_wComposedType);
	_m_CHECK_FIELD(m_wOutputComposedType);
	_m_CHECK_FIELD(m_wCRC16Init);
	_m_CHECK_FIELD(m_wCPAddr);
	_m_CHECK_FIELD(m_wPUAddr);
	_m_CHECK_FIELD(m_mapMsgTypes);
	_m_CHECK_FIELD(m_mapMsgTypesToUnpack);
	_m_CHECK_FIELD(m_bUnpackAll);
	_m_CHECK_FIELD(m_mapMsgTypesToMark);
	_m_CHECK_FIELD(m_bMarkAll);
	_m_CHECK_FIELD(m_nStatusPeriod);
	_m_CHECK_FIELD(m_iSendStatTO);
	_m_CHECK_FIELD(m_StatusHdr);
	_m_CHECK_FIELD(m_StatusMsg);
	_m_CHECK_FIELD(m_MarkNestedMask);
	_m_CHECK_FIELD(m_MarkComposedMask);
	_m_CHECK_FIELD(m_TUType);
	_m_CHECK_FIELD(m_TUSrcMask);
	_m_CHECK_FIELD(m_TUSrcComMsgIndex);
	_m_CHECK_FIELD(m_TUPrimToSecSrc);
	_m_CHECK_FIELD(m_TUSecToPrimSrc);
	_m_CHECK_FIELD(m_arStatusData);
#if 0 // time-dependent
	_m_CHECK_FIELD(m_arStatusMsg);
#endif
	_m_CHECK_FIELD(m_bKeepLog);
	_m_CHECK_FIELD(m_wLogComposedType);
	_m_CHECK_FIELD(m_mapLogMsgTypesToUnpack);
	_m_CHECK_FIELD(m_bLogUnpackAll);
	_m_CHECK_FIELD(m_wLogComposedTypeToPack);
	_m_CHECK_FIELD(m_mapLogMsgTypesToPack);
	_m_CHECK_FIELD(m_bLogPackAll);
	_m_CHECK_FIELD(m_wSourceID);
	_m_CHECK_FIELD(m_wStatusRequestMessageType);
#undef _m_CHECK_FIELD
}

static void compare_lines(const tstring &pre_line, const tstring &post_line) {
	ASSERT_RELATION(post_line.length(), LE, pre_line.length()) << "The line in the post-file is longer.";
	ASSERT_RELATION(post_line.length(), GE, pre_line.length()) << "The line in the post-file is shorter.";
	EXPECT_RELATION(post_line, EQ, pre_line) << "Mismatch at " << longest_common_prefix(pre_line, post_line) << ".";
}

static void compare_files(const LPCTSTR pre_filename, const LPCTSTR post_filename, const bool return_on_fatal) {
	tifstream  pre_file( pre_filename);
	tifstream post_file(post_filename);
	tstring  pre_line;
	tstring post_line;
	for (std::streamoff line_no = 1; getline(pre_file, pre_line); line_no++) {
		assert(!pre_file.bad());
		assert(!pre_file.fail());
		SCOPED_TRACE(testing::Message() << "On line #" << line_no << ".");
		getline(post_file, post_line);
		PREREQUIRE_FALSE(post_file.bad()) << "I/O error on post-file.";
		if (post_file.fail()) {
			PREREQUIRE_TRUE(post_file.eof()) << "Formatting error on post-file.";
			ASSERT_TRUE(pre_file.eof()) << "The post-file has less lines.";
		}
		compare_lines(pre_line, post_line);
		if (testing::Test::HasFatalFailure() && return_on_fatal)
			return;
	}
	PREREQUIRE_FALSE(pre_file.bad()) << "I/O error on pre-file.";
	assert(pre_file.fail());
	PREREQUIRE_TRUE(pre_file.eof()) << "Formatting error on pre-file.";
	ASSERT_FALSE(getline(post_file, post_line)) << "The post-file has more lines.";
}

TEST_F(SettingsFixture, Load_Save_CanonicalRoundtrip) {
	const TCHAR original_filepath[] = _T("data/canonical-saved.ini");
	const TCHAR  resaved_filepath[] = _T(STRINGIFY(CONFIG)"/_m_resaved.ini");
	LOAD_AND_ASSERT_SUCCESS(settings, original_filepath);
	SAVE_AND_ASSERT_SUCCESS(settings,  resaved_filepath);
	compare_files(original_filepath, resaved_filepath, /* return_on_fatal = */ false);
}

TEST_F(SettingsFixture, Load_Save_RegularRoundtrip) {
	const TCHAR original_filepath[] = _T("data/regular.ini");
	const TCHAR  resaved_filepath[] = _T(STRINGIFY(CONFIG)"/_m_resaved.ini");
	LOAD_AND_ASSERT_SUCCESS(settings, original_filepath);
	SAVE_AND_ASSERT_SUCCESS(settings,  resaved_filepath);
	compare_files(original_filepath, resaved_filepath, /* return_on_fatal = */ true);
}

/*
 0 \_ m_StatusHdr.m_wMaxLength
 1 /
 2 \_ m_StatusHdr.m_wType
 3 /
 4 \_ m_StatusHdr.m_wDestination
 5 /
 6 \_ m_StatusHdr.m_wSource
 7 /
 8 \
 9 |_ time(NULL)
10 |
11 /
12 >- ind
13 >- 0
14 \_ m_StatusMsg.m_wMaxLength
15 /
16 \_ m_StatusMsg.m_wType
17 /
18 \_ m_StatusMsg.m_wDestination
19 /
20 \_ m_StatusMsg.m_wSource
21 /
22 \
23 |_ time(NULL)
24 |
25 /
26 >- ind
27 >- 0
28 \
.. |_ m_arStatusData
.. |
-5 /
-4 \_ CRC16(14:-4)
-3 /
-2 \_ CRC16(0:-2)
-1 /
*/

static void check_message(const CSettings &settings, const bool check_updatables, const unsigned char ind = 0) {
	assert(settings.m_arStatusMsg.GetSize() >= 0);
	assert(settings.m_arStatusData.GetSize() >= 0);
	ASSERT_RELATION(settings.m_StatusMsg.m_wMaxLength, EQ, 16 + settings.m_arStatusData.GetSize());
	ASSERT_RELATION(settings.m_StatusHdr.m_wMaxLength, EQ, 16 + settings.m_StatusMsg.m_wMaxLength);
	ASSERT_RELATION(settings.m_arStatusMsg.GetSize(), EQ, settings.m_StatusHdr.m_wMaxLength);
	ASSERT_RELATION(settings.m_arStatusMsg.GetSize(), EQ, 28 + settings.m_arStatusData.GetSize() + 4);

	const BYTE * const bytes = settings.m_arStatusMsg.GetData();

	EXPECT_RELATION(binfield<WORD>(bytes, 0), EQ, settings.m_StatusHdr.m_wMaxLength);
	EXPECT_RELATION(binfield<WORD>(bytes, 2), EQ, settings.m_StatusHdr.m_wType);
	EXPECT_RELATION(binfield<WORD>(bytes, 4), EQ, settings.m_StatusHdr.m_wDestination);
	EXPECT_RELATION(binfield<WORD>(bytes, 6), EQ, settings.m_StatusHdr.m_wSource);
	if (check_updatables) {
		const time_t t = time(NULL);
		ASSERT_RELATION(t, GE, (time_t)(0));
		EXPECT_RELATION(binfield<DWORD>(bytes,  8), GE, 0U);
		EXPECT_RELATION(binfield<DWORD>(bytes,  8), LE, static_cast<DWORD>(t));
		EXPECT_RELATION(binfield< BYTE>(bytes, 12), EQ, ind);
	}
	EXPECT_RELATION(binfield<BYTE>(bytes, 13), EQ, 0);

	EXPECT_RELATION(binfield<WORD>(bytes, 14), EQ, settings.m_StatusMsg.m_wMaxLength);
	EXPECT_RELATION(binfield<WORD>(bytes, 16), EQ, settings.m_StatusMsg.m_wType);
	EXPECT_RELATION(binfield<WORD>(bytes, 18), EQ, settings.m_StatusMsg.m_wDestination);
	EXPECT_RELATION(binfield<WORD>(bytes, 20), EQ, settings.m_StatusMsg.m_wSource);
	if (check_updatables) {
		const time_t t = time(NULL);
		ASSERT_RELATION(t, GE, (time_t)(0));
		EXPECT_RELATION(binfield<DWORD>(bytes, 22), GE, 0U);
		EXPECT_RELATION(binfield<DWORD>(bytes, 22), LE, static_cast<DWORD>(t));
		EXPECT_RELATION(binfield< BYTE>(bytes, 26), EQ, ind);
	}
	EXPECT_RELATION(binfield<BYTE>(bytes, 27), EQ, 0);

	const std::size_t n = settings.m_arStatusData.GetSize();
	for (std::size_t i = 0; i < n; i++) {
		EXPECT_RELATION(binfield<BYTE>(bytes, 28 + i), EQ, binfield<BYTE>(settings.m_arStatusData.GetData(), i))
			<< "i = " << i << ".";
	}
	if (check_updatables) {
		EXPECT_RELATION(binfield<WORD>(bytes, 28 + n    ), EQ, CRC16(const_cast<BYTE *>(bytes + 14), 14 + n    , settings.m_wCRC16Init));
		EXPECT_RELATION(binfield<WORD>(bytes, 28 + n + 2), EQ, CRC16(const_cast<BYTE *>(bytes +  0), 28 + n + 2, settings.m_wCRC16Init));
	}
}

class SettingsWithDataFixture: public SettingsFixture {
protected:
	SettingsWithDataFixture() {
		fill_with_some_data(settings.m_arStatusData);
	}
};

class SettingsWithMessageMadeFixture: public SettingsWithDataFixture {
protected:
	SettingsWithMessageMadeFixture() {
		settings.MakeStatusMsg();
	}
};

TEST_F(SettingsWithDataFixture, MakeStatusMsg_ContentsCheck) {
	settings.MakeStatusMsg();
	check_message(settings, /* check_updatables = */ false);
}

TEST_F(SettingsWithDataFixture, MakeStatusMsg_UpdateStatusMsg_FullContentsCheck) {
	const unsigned char ind = 0;
	settings.MakeStatusMsg();
	settings.UpdateStatusMsg(ind);
	check_message(settings, /* check_updatables = */ true, ind);
}

TEST_F(SettingsWithMessageMadeFixture, UpdateStatusMsg_TimeDependance) {
	CByteArray old_msg;

	settings.UpdateStatusMsg(0);
	old_msg.Copy(settings.m_arStatusMsg);
	Sleep(1000);
	settings.UpdateStatusMsg(0);

	EXPECT_RELATION(settings.m_arStatusMsg.GetSize(), EQ, old_msg.GetSize());
	EXPECT_RELATION(settings.m_arStatusMsg, NE, old_msg);
}

TEST_F(SettingsWithMessageMadeFixture, UpdateStatusMsg_IndDependance) {
	const unsigned na = 100;

	for (unsigned a = 0; a < na; a++) {
		SCOPED_TRACE(testing::Message() << "Temporally close updates, attempt #" << a << ".");

		CByteArray old_msg;

		{	const time_t t1 = time(NULL);
			settings.UpdateStatusMsg(/* ind = */ 0);
			old_msg.Copy(settings.m_arStatusMsg);
			settings.UpdateStatusMsg(/* ind = */ 1);
			const time_t t2 = time(NULL);
			if (t1 < 0 || t2 < 0)
				INCONCLUSIVE() << "Timing failed.";
			if (t2 != t1)
				continue;
		}

		EXPECT_RELATION(settings.m_arStatusMsg.GetSize(), EQ, old_msg.GetSize());
		EXPECT_RELATION(settings.m_arStatusMsg, NE, old_msg);
		return;
	}

	INCONCLUSIVE() << "Given up trying to perform temporally close updates after " << na << " attempts.";
}

TEST_F(SettingsFixture, MakeStatusMsg_UpdateStatusMsg_DataDependance) {
	const unsigned na = 100;

	for (unsigned a = 0; a < na; a++) {
		SCOPED_TRACE(testing::Message() << "Temporally close updates, attempt #" << a << ".");

		CByteArray old_msg;

		{	const unsigned char ind = 0;
			const time_t t1 = time(NULL);
			fill_with_some_data(settings.m_arStatusData, /* seed = */ 0);
			settings.MakeStatusMsg();
			settings.UpdateStatusMsg(ind);
			old_msg.Copy(settings.m_arStatusMsg);
			fill_with_some_data(settings.m_arStatusData, /* seed = */ 1);
			settings.MakeStatusMsg();
			settings.UpdateStatusMsg(ind);
			const time_t t2 = time(NULL);
			if (t1 < 0 || t2 < 0)
				INCONCLUSIVE() << "Timing failed.";
			if (t2 != t1)
				continue;
		}

		EXPECT_RELATION(settings.m_arStatusMsg.GetSize(), EQ, old_msg.GetSize());
		EXPECT_RELATION(settings.m_arStatusMsg, NE, old_msg);
		return;
	}

	INCONCLUSIVE() << "Given up trying to perform temporally close updates after " << na << " attempts.";
}

} // namespace Tests
} // namespace
